<!-- ***********  Required Prerequisites **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
	<tr>
		<td align="left" valign="top" bgcolor="#0080C0"><font
			face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">Prerequisites
				and Handy Extras</font></td>
	</tr>
	<tr>
		<td>
			<p>These are the prerequisites to build or run these packages. All
				are not necessarily required, but instead some subset. Also listed
				are some frequently needed links for committer-required packages
				when creating new development environments, or targets to run
				against.</p>
			<p>
				Note that Java 8 or later is recommended for using WTP at a whole, even though subsets of WTP
				and other <a href="http://www.eclipse.org/downloads/">Eclipse
					Projects</a> might run with <a
					href="https://wiki.eclipse.org/WTP_Releng_BREEs">other JRE
					levels</a>. See <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools#target_environments">planned target environments</a> for more information.
			</p>
			<p></p>
		</td>
	</tr>
	<tr>
		<td>
			<table border=0 cellspacing=1 cellpadding=2 width="80%"
				align="center">

                <tr valign="middle">
                    <td colspan="2"><hr /></td>
                </tr>

                <tr valign="middle">
                    <th align="left">Update Sites linked from <a href=wtp-parent_pom.xml>parent pom</a></th>
                    <th align="right">Zips/Download Links</th>
                </tr>

                    <?php
                    // The source file for dependency.properties.php is in ../data/dependencies.properties.
                    // Ant will convert this into a file with PHP vars in wtp-parent/postBuild.xml line 487, writePropertiesAsPHP
                    // See index.html.template.php for how to regenerate this section locally to test it.
                    include_once('dependency.properties.php');
                    include_once('prereqsToDisplay.php');
                    $eclipse_mirror_script="http://www.eclipse.org/downloads/download.php?file=";
                    ?>

                    <?php

                    if ("true" === $prereq_e4x) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $e4x_name . " " . $e4x_description ;
                        echo "</td>";
                        //customize page depending on user's browser/platform, if we can detect it
                        $usersPlatform = getPlatform();
                        // assume windows by default, since likely most frequent, even for cases where
                        // platform is "unknown". I've noticed Opera reports 'unknown' :(
                        $recommendedFile=$e4x_file_win32_win32_x86_64;
                        if (strcmp($usersPlatform,"linux")== 0) {
                            $recommendedFile=$e4x_eclipse_file_linux_gtk_x86_64;
                        } else if (strcmp($usersPlatform,"mac") == 0) {
                            $recommendedFile=$e4x_eclipse_file_macosx_cocoa_x86_64;
                        }
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $e4x_mirror_prefixuri, $e4x_url, $recommendedFile, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $e4x_url . "\">appropriate platform</a>";
                        echo " or <a href=\"" . $e4x_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_dtp) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $dtp_name . " " . $dtp_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $dtp_mirror_prefixuri, $dtp_url, $dtp_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $dtp_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_eclipselink) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $eclipselink_name  . " " . $eclipselink_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $eclipselink_mirror_prefixuri, $eclipselink_url, $eclipselink_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $eclipselink_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_egit) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $egit_name  . " " . $egit_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $egit_mirror_prefixuri, $egit_url, $egit_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $egit_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_emfandxsd) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $emfandxsd_name . " " . $emfandxsd_description ;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $emfandxsd_mirror_prefixuri, $emfandxsd_url, $emfandxsd_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $emfandxsd_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_emftransaction) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $emftransaction_name  . " " . $emftransaction_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $emftransaction_mirror_prefixuri, $emftransaction_url, $emftransaction_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $emftransaction_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_emfvalidation) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $emfvalidation_name  . " " . $emfvalidation_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $emfvalidation_mirror_prefixuri, $emfvalidation_url, $emfvalidation_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $emfvalidation_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_graphiti) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $graphiti_name  . " " . $graphiti_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $graphiti_mirror_prefixuri, $graphiti_url, $graphiti_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $graphiti_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                    if ("true" === $prereq_jetty) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $jetty_name  . " " . $jetty_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $jetty_mirror_prefixuri, $jetty_url, $jetty_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $jetty_build_home . "\">equivalent</a></td>";
                        echo " </tr>";
                    }

                     if ("true" === $prereq_orbitthirdpartyzip) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $orbitthirdpartyzip_name . " " . $orbitthirdpartyzip_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $orbitthirdpartyzip_mirror_prefixuri, $orbitthirdpartyzip_url, $orbitthirdpartyzip_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $orbitthirdpartyzip_build_home . "\">equivalent</a></td>";
                        echo "</tr>";
                     }

                    if ("true" === $prereq_swtbot) {
                        echo "<tr valign=\"top\" bgcolor='#EEEEEE'>";
                        echo "<td width=\"50%\">";
                        echo $swtbot_name  . " " . $swtbot_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo getPrereqReferenceOrName($eclipse_mirror_script, $swtbot_mirror_prefixuri, $swtbot_url, $swtbot_file, $eclipse_fspath_prefix);
                        echo " or <a href=\"" . $swtbot_build_home . "\">equivalent</a></td>";

                        echo " </tr>";
                    }

                    if ("true" === $prereq_gef) {
                        echo "<tr valign=\"top\">";
                        echo "<td width=\"50%\">";
                        echo $gef_name  . " " . $gef_description;
                        echo "</td>";
                        echo "<td align=\"right\">";
                        echo "<a href=\"" . $gef_build_home . "\">downloads</a></td>";
                        echo " </tr>";
                    }

                     ?>

                <tr valign="middle">
                    <td colspan="2"><hr /></td>
                </tr>

              </table>
		</td>
	</tr>
</table>

