<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<!-- generated from webtools.releng.aggregator/wtp-parent/templateFiles/index.html.template.php -->

<?php
/*
To test local changes to this page or when dependencies change, run this:

  cd webtools.releng.aggregator/wtp-parent; ant -f postBuild.xml -lib ./libs/ init prepare-page-files -Dpublish=false -Dbuild.type=I \
  -Dwtp.version=3.12.0 -Dwtp.committers=/opt/lampp/htdocs/tmp -Dbuild.id=20201212012345

Then start a xampp apache server on localhost:80 and go to http://localhost/tmp/wtp-R3.12.0-I/20201212012345/I-3.12.0-20201212012345/
*/

//ini_set("display_errors", "true");
//error_reporting (E_ALL);

include_once("miscUtil.php");
include_once('buildvariables.php');

// The source file for dependency.properties.php is in ../data/dependencies.properties.
// Ant will convert this into a file with PHP vars in wtp-parent/postBuild.xml line 487, <writePropertiesAsPHP>
include_once('dependency.properties.php');

// our summary results handling requires php 5 (for simple xml file loading)
// so, if not php 5, just don't display any summary results
// This was found to be required, since some mirror our whole site (e.g. IBM)
// and not all mirrors use PHP 5
$displayTestSummary=false;
if (phpversion() >= 5) {

     $code_totalBundles=0;
     $code_totalErrors=0;
     $code_totalWarnings=0;
     $code_totalforbiddenAccessWarningCount=0;
     $code_totaldiscouragedAccessWarningCount=0;

     $test_totalBundles=0;
     $test_totalErrors=0;
     $test_totalWarnings=0;
     $test_totalforbiddenAccessWarningCount=0;
     $test_totaldiscouragedAccessWarningCount=0;


     $displayTestSummary=true;
     // expecting grandTotalErrors and grandTotalTests
     $filename = "unitTestsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "unitTests_";
          $unitTestsSummary = simplexml_load_file($filename);
          foreach ($unitTestsSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "compilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "code_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }

     $filename = "testcompilelogsSummary.xml";
     if (file_exists($filename)) {
          $prefix = "test_";
          $compileSummary = simplexml_load_file($filename);
          foreach ($compileSummary->summaryItem as $summaryItem) {
               $name = $summaryItem->name;
               $value = $summaryItem->value;
               $code= "\$" . $prefix . $name . " = " . $value . ";";
               //echo "<br />code: " . $code;
               eval($code);
          }
     }
}


?>

<?php include 'headingIntro.php';?>
<?php include 'displayPrereqs.php';?>

<!-- ***********  P2 Zips **************  -->



<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top colspan="2" bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">p2 repos (update sites)</font></td>
     </tr>

     <tr>
          <td align="left" valign="top" colspan="5">
          <p>These are p2 repositories (or zips of repos). Zips can be
          downloaded and installed into a development environment or PDE target.
          <br/>Do not simply unzip these - you should use Eclipse / p2 to perform an installation instead.</p>
          </td>
     </tr>

     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

          <?php

          $shortname=$build_distribution."-repo";

          $zipfilename=$shortname."-".$build;
          $filename=$zipfilename.".zip";
          echo "<!-- 
shortname = ${shortname}
build_distribution = ${build_distribution}
build = ${build}
zipfilename = ${zipfilename}
filename = ${filename}
-->";
          if (file_exists($filename)) {
               ?>
               <tr>
                    <td align="left" valign="top" width="10%"><b><a href=repository>Code Repository</a></b></td>
                    <td align="left" valign="top">
                    <p>Archived <a href=repository>p2 repository of WTP code</a>. Good for product builders. </p>
                    </td>
                    <?php
                    $zipfilesize=fileSizeForDisplay($filename);

                    displayFileLine($downloadprefix, $filename, $zipfilesize, $shortname);
                    ?>
               </tr>
               <?php } else {
                        echo "<!-- ERROR: could not find ${filename} -->";
                     }  ?>
              <?php

              $shortname=$build_distribution."-tests-repo";

              $zipfilename=$shortname."-".$build;
              $filename=$zipfilename.".zip";
              echo "<!-- 
shortname = ${shortname}
build_distribution = ${build_distribution}
build = ${build}
zipfilename = ${zipfilename}
filename = ${filename}
-->";
              if (file_exists($filename)) {
                     ?>
                     <tr>
                            <td align="left" valign="top" width="10%"><b><a href=repositoryunittests>Tests Repository</a></b></td>
                            <td align="left" valign="top">
                            <p>Archived <a href=repositoryunittests>p2 repository of WTP unit tests</a>.</p>
                            </td>
                            <?php
                            $zipfilesize=fileSizeForDisplay($filename);

                            displayFileLine($downloadprefix, $filename, $zipfilesize, $shortname);
                            ?>
                     </tr>
               <?php } else {
                        echo "<!-- ERROR: could not find ${filename} -->";
                     }  ?>
          </table>

     </tr>
</table>

               <?php include 'webdev.php'; ?>
               <?php include 'fproject.php'; ?>

<!-- ***********  Build Status **************  -->
<table border=0 cellspacing=2 cellpadding=2 width="100%">
     <tr>
          <td align=left valign=top bgcolor="#0080C0"><font
               face="'Bitstream Vera',Helvetica,Arial" color="#FFFFFF">Status, tests
          and other interesting details</font></td>
     </tr>
     <tr>
          <td>
          <table border=0 cellspacing=2 cellpadding=2 width="90%" align="center">

               <tr>
                    <td><?php 
                    if (isset($displayBuildNotes) && $displayBuildNotes) {
                         echo "<a href=\"buildNotes.php\">Build notes</a> <br />";
                    }
                    ?>

		   <!--<a href="directory.txt">map files</a> <br />-->
                    <?php

                    if (file_exists("components")) {
                         echo "<a href=\"components/components.php\">Misc Components</a> <br />\n";
                    }


                    if ($displayTestSummary) {

                         // we may really be pending (tests still running) or maybe they failed in unexpted way?.
                         echo "<br /><font color=\"black\">See latest <a href=\"https://ci.eclipse.org/webtools/view/webtools_CI/\">CI builds here</a> for unit tests per component.</font>";

                         $fileName = "fullLog.zip";
                         if (file_exists($fileName)) {
                            echo "<br /> <a href='./$fileName'/>Full Log</a> from build ";
                         }
                    }

                    ?> <br />

                    <?php
                    if (file_exists("versioningReportName.php")) {
                         include "versioningReportName.php";
                         $fname=$versionReportFilename.".html";
                         if (file_exists($fname)) {
                              echo "<br /> <a href='$fname'>Versioning Information</a>";
                         }
                    }
                    ?></td>
               </tr>
          </table>
          </td>
     </tr>
</table>

<!-- footer -->
<center>
<hr>
<p>All downloads are provided under the terms and conditions of the <a
     href="http://www.eclipse.org/legal/notice.html">Eclipse.org Software
User Agreement</a> unless otherwise specified.</p>

<p>If you have problems downloading the drops, contact the <font
     face="'Bitstream Vera',Helvetica,Arial" size="-1"><a
     href="mailto:webmaster@eclipse.org">webmaster</a></font>.</p>

</center>
<!-- end footer -->

</body>
</html>
