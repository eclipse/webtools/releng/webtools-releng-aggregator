# this file is a parser rule config file for use with the Jenkins plugin log-parser
# for more info see https://plugins.jenkins.io/log-parser
# if a line matches multiple times, the FIRST match will apply

# to use this file in a Jenkins job:
# 1. run a shelll script to get the file
#    wget http://git.eclipse.org/c/webtools/webtools.releng.aggregator.git/plain/scripts/webtools-console-log-parser.txt -O ${WORKSPACE}/jbosstools-console-log-parser.txt
# 2. In Post-Build Actions, enable "Console output (build log) parsing".
# 3. Check "Use project rule", then set "Path to rule file in workspace" = ${WORKSPACE}/jbosstools-console-log-parser.txt

# warnings/errors to ignore
ok /ignoring option .*PermSize/
ok /No baseline version/
ok /Could not transfer metadata org.jboss.tools:parent/
ok /Failure to transfer org.jboss.tools:parent/
ok /Couldn't create directory: Failure/

# section starts: these are used to group errors and warnings found after the line; also creates a quick access link.
start /Reactor Build Order/
start /\[INFO\] Building /
start /tycho-surefire-plugin:.+:test \(.+test\).+/
start /T E S T S/
start /Reactor Summary/
start /checkLatestPublishedSHA.sh/ 
start /found ID = /

# quick access links to these console log lines
info /tycho\.testArgLine/
info /\[INFO\] All tests passed/
info /Tests run:.+/
info /BUILD SUCCESS/
info /Total time/
info /Latest build for /
info /rsync -arz --rsh=ssh --protocol=28/
info /\[INFO\] .+ DONE: .+/

# warnings
warning /\[p2.mirror\] .+/ 
warning /.+\[WARNING\].+/
warning /[Ww]arning/
warning /WARNING/
warning /Connection refused/
warning /Skipped: [123456789]/
warning /COMPOSITE UNCHANGED/

# these might need to be errors
warning /Unable to read repository/
warning /Connection closed by remote host/
warning /Connection reset by peer/
warning /Couldn't read packet/
warning /ln: failed to create symbolic link/
warning /.settings\/org.eclipse.jdt.core.prefs/

# errors
error /HTTPError/
error / \[ERROR\] /
error /BUILD FAILED/
error /No latest build found/

error /exec returned: 13/
error /rsync error/

error /FAILURE IN OUTPUT/
error /failed:/
error /No such file or directory/
error /No repository found/

error /Problems resolving provisioning plan/
error /Validation found errors/

error /Cannot complete the install/
error /one or more required items could not be found/
error /no installation folders found/

error /Unable to satisfy dependency/
error /Cannot satisfy dependency/
error /conflicting dependency/
error /Could not resolve content/
error /Missing requirement/

error /The following error occurred/
# match line starting with 'error ', case-insensitive
error /(?i)^error /

